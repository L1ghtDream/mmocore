package net.Indyuce.mmocore.player.playerclass;

public enum ClassTriggerType {
    BREAK_BLOCK,

    PLACE_BLOCK,

    CLASS_CHOSEN,

    LEVEL_UP,

    @Deprecated
    MAGIC_DAMAGE,

    @Deprecated
    PHYSICAL_DAMAGE,

    @Deprecated
    PROJECTILE_DAMAGE,

    @Deprecated
    WEAPON_DAMAGE,

    @Deprecated
    SKILL_DAMAGE,

    @Deprecated
    UNARMED_DAMAGE;
}
